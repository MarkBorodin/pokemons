import json
import pytest
import socket as s
import requests


@pytest.yield_fixture
def socket():
    _socket = s.socket(s.AF_INET, s.SOCK_STREAM)
    yield _socket
    _socket.close()


def test_server_connect(socket):
    socket.connect(('localhost', 8080))
    assert socket


def test_handler():

    def get_item(url):
        response = requests.get(url)
        resp = response.json()
        return resp

    def post(item):
        r = requests.post('http://0.0.0.0:8080/post', data=item)
        return r.text

    url1 = 'https://pokeapi.co/api/v2/pokemon/7/'
    url2 = 'https://pokeapi.co/api/v2/pokemon/8/'
    url3 = 'https://pokeapi.co/api/v2/pokemon/13/'

    # middle
    item1 = get_item(url1)
    item1 = json.dumps(item1)
    resp = post(item1)
    resp = json.loads(resp)
    assert len(resp) == 2
    assert resp['id']
    assert resp['name']
    assert post(item1) == '{"id": 7, "name": "squirtle"}'

    # boss
    item2 = get_item(url2)
    item2 = json.dumps(item2)
    resp = post(item2)
    resp = json.loads(resp)
    assert len(resp) == 2
    assert resp['id']
    assert resp['name']
    assert resp['name'][-9:] == '_the_boss'
    assert post(item2) == '{"id": 8, "name": "wartortle_the_boss"}'

    # like_a_feather
    item3 = get_item(url3)
    item3 = json.dumps(item3)
    resp = post(item3)
    resp = json.loads(resp)
    assert len(resp) == 2
    assert resp['id']
    assert resp['name']
    assert resp['name'][:15] == 'like_a_feather_'
    assert post(item3) == '{"id": 13, "name": "like_a_feather_weedle"}'

    # middle, not equal
    item1 = get_item(url1)
    item1 = json.dumps(item1)
    resp = post(item1)
    resp = json.loads(resp)
    assert len(resp) == 2
    assert resp['id']
    assert resp['name']
    assert post(item1) != '{"id": 0, "name": "squirtle"}'
