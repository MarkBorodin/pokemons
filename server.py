import json
import aiohttp

from aiohttp import web


async def handler(request):
    result = await request.json()
    max_weight = 100
    min_weight = 50
    if result['weight'] > max_weight:
        name = result['name'] + '_the_boss'
    elif result['weight'] < min_weight:
        name = 'like_a_feather_' + result['name']
    else:
        name = result['name']
    pokemon = {
        'id': result['id'],
        'name': name
    }
    pokemon = json.dumps(pokemon)
    return aiohttp.web.Response(
        body=pokemon
    )


app = web.Application()
app.add_routes([web.post('/post', handler)])


if __name__ == '__main__':
    web.run_app(app)
