import json
import re
import aiohttp
import asyncio
import requests


url = 'https://pokeapi.co/api/v2/pokemon?limit=50'


def get_urls(url):
    response = requests.get(url)
    resp = response.json()
    urls_list = []
    for result in resp['results']:
        urls_list.append(result['url'])
    return urls_list


urls = get_urls(url)


sent_pokemons = []
filtered_pokemons = []


async def get(url, sent_pokemons, filtered_pokemons):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            item = await response.json()
            types = len(item['types'])
            if types > 1 and (len(re.findall(r'grass', str(item['types']))) > 0 or len(re.findall(r'fire', str(item['types']))) > 0):
                filtered_pokemons.append(item)
                print(item)
            else:
                sent_pokemons.append(item)
                print(item)


loop = asyncio.get_event_loop()
coroutines = []
for link in urls:
    coroutines.append(get(link, sent_pokemons, filtered_pokemons))
loop.run_until_complete(asyncio.gather(*coroutines))


async def send(sent_pokemons):
    async with aiohttp.ClientSession() as session:
        for pokemon in sent_pokemons:
            pokemon = json.dumps(pokemon)
            async with session.post('http://0.0.0.0:8080/post', data=pokemon) as resp:
                print(await resp.text())


loop = asyncio.get_event_loop()
loop.run_until_complete(send(sent_pokemons))
